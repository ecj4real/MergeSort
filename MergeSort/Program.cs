﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter number to be sorted seperating each number by a comma:");
            string[] input = (Console.ReadLine()).Split(',');

            int[] unsorted = new int[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                unsorted[i] = Convert.ToInt32(input[i]);
            }


            int[] sorted = merge(unsorted);

            Console.WriteLine("\n\nThe Sorted List is: ");
            foreach (int num in sorted)
            {
                Console.Write(num + "\t");
            }
            Console.ReadKey();
        }

        public static int[] merge(int[] unsorted)
        {
            int lengthOfUnsorted = unsorted.Length;

            if(lengthOfUnsorted == 1)
            {
                return unsorted;
            }

            int[] firstHalf = new int[lengthOfUnsorted/2]; 
            Array.Copy(unsorted, 0, firstHalf, 0, lengthOfUnsorted/2);
            int[] secondHalf = new int[lengthOfUnsorted - (lengthOfUnsorted / 2)];
            Array.Copy(unsorted, lengthOfUnsorted/2, secondHalf, 0, secondHalf.Length);

            firstHalf = merge(firstHalf);
            secondHalf = merge(secondHalf);

            return sort(firstHalf, secondHalf);
        }

        public static int[] sort(int[] a, int[] b)
        {
            List<int> partA = new List<int>();
            List<int> partB = new List<int>();

            List<int> output = new List<int>();

            for (int i = 0; i < a.Count(); i++)
            {
                partA.Add(a[i]);
            }
            for (int i = 0; i < b.Count(); i++)
            {
                partB.Add(b[i]);
            }

            while (partA.Any() && partB.Any())
            {
                if (partA[0] > partB[0])
                {
                    output.Add(partB[0]);
                    partB.RemoveAt(0);
                }
                else
                {
                    output.Add(partA[0]);
                    partA.RemoveAt(0);
                }
            }

            while (partA.Any())
            {
                output.Add(partA[0]);
                partA.RemoveAt(0);
            }

            while (partB.Any())
            {
                output.Add(partB[0]);
                partB.RemoveAt(0);
            }
            int[] sorted = output.ToArray();

            return sorted;
        }
    }
}
